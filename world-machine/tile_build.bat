magick montage -geometry +0+0 ^
	outputs\heightmap_x0_y3.tif ^
	outputs\heightmap_x1_y3.tif ^
	outputs\heightmap_x2_y3.tif ^
	outputs\heightmap_x3_y3.tif ^
	outputs\heightmap_x0_y2.tif ^
	outputs\heightmap_x1_y2.tif ^
	outputs\heightmap_x2_y2.tif ^
	outputs\heightmap_x3_y2.tif ^
	outputs\heightmap_x0_y1.tif ^
	outputs\heightmap_x1_y1.tif ^
	outputs\heightmap_x2_y1.tif ^
	outputs\heightmap_x3_y1.tif ^
	outputs\heightmap_x0_y0.tif ^
	outputs\heightmap_x1_y0.tif ^
	outputs\heightmap_x2_y0.tif ^
	outputs\heightmap_x3_y0.tif ^
	outputs\heightmap_tiled.tif
	
	magick montage -geometry +0+0 ^
	outputs\rivers_x0_y3.png ^
	outputs\rivers_x1_y3.png ^
	outputs\rivers_x2_y3.png ^
	outputs\rivers_x3_y3.png ^
	outputs\rivers_x0_y2.png ^
	outputs\rivers_x1_y2.png ^
	outputs\rivers_x2_y2.png ^
	outputs\rivers_x3_y2.png ^
	outputs\rivers_x0_y1.png ^
	outputs\rivers_x1_y1.png ^
	outputs\rivers_x2_y1.png ^
	outputs\rivers_x3_y1.png ^
	outputs\rivers_x0_y0.png ^
	outputs\rivers_x1_y0.png ^
	outputs\rivers_x2_y0.png ^
	outputs\rivers_x3_y0.png ^
	outputs\rivers_tiled.png